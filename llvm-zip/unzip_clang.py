#!/usr/bin/env python
# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""This script is used to download prebuilt clang binaries.

It is also used by package.py to build the prebuilt clang binaries."""

import argparse
import distutils.spawn
import glob
import os
import pipes
import re
import shutil
import subprocess
import stat
import sys
import zipfile

THIS_DIR = os.path.abspath(os.path.dirname(__file__))
CHROMIUM_DIR = os.path.abspath(os.path.join(THIS_DIR, '..', '..'))
LLVM_ZIP = os.path.join(CHROMIUM_DIR, 'third_party', 'llvm-zip')
LLVM_BUILD_DIR = os.path.join(CHROMIUM_DIR, 'third_party', 'llvm-build')
LLVM_BUILD_BIN_DIR = os.path.join(CHROMIUM_DIR, 'third_party', 'llvm-build', 'Release+Asserts', 'bin')

def UnZip():
  LLVM_ZIP_PATH = ""
  if sys.platform == 'win32':
    LLVM_ZIP_PATH = os.path.join(LLVM_ZIP, 'llvm_win32.zip')
  elif sys.platform == 'darwin':
    LLVM_ZIP_PATH = os.path.join(LLVM_ZIP, 'llvm_darwin.zip')
  elif sys.platform == 'linux2':
    LLVM_ZIP_PATH = os.path.join(LLVM_ZIP, 'llvm_linux.zip')
  else:
    print 'unknown os:' + sys.platform
    return -1;
  print 'extract ' + sys.platform + ' clang build file ...'
  f = zipfile.ZipFile(LLVM_ZIP_PATH, 'r')
  for file in f.namelist():
    f.extract(file, LLVM_BUILD_DIR)
  if sys.platform == 'darwin':
    CLANG_FILE_PATH = os.path.join(LLVM_BUILD_BIN_DIR, 'clang')
    CLANGPLUS_FILE_PATH = os.path.join(LLVM_BUILD_BIN_DIR, 'clang++')
    CLANGCL_FILE_PATH = os.path.join(LLVM_BUILD_BIN_DIR, 'clang-cl')
    os.system('rm' + ' ' + CLANGPLUS_FILE_PATH + ' ' + CLANGCL_FILE_PATH)
    os.system('chmod a+x' + ' ' + CLANG_FILE_PATH )
    os.system('ln -s' + ' ' +  CLANG_FILE_PATH + ' ' + CLANGPLUS_FILE_PATH)
    os.system('ln -s' + ' ' + CLANG_FILE_PATH + ' ' + CLANGCL_FILE_PATH)
  print 'extract clang build file finish'
  return 0;

def main():
  parser = argparse.ArgumentParser(description='Unzip Clang.')
  print sys.platform
  if (sys.platform == 'win32') or (sys.platform == 'darwin') or (sys.platform == 'linux2'):
    return UnZip()
  return -1;

if __name__ == '__main__':
  sys.exit(main())
